ARG STRAPI_VERSION=3.6.5
FROM strapi/base:14
ARG STRAPI_VERSION

RUN mkdir /srv/app && chown 1000:1000 -R /srv/app

WORKDIR /srv/app

ENV NODE_ENV production

RUN yarn global add strapi@${STRAPI_VERSION} && \
    strapi new . \
      --dbclient=postgres \
      --dbhost=$DATABASE_HOST \
      --dbport=$DATABASE_PORT \
      --dbname=$DATABASE_NAME \
      --dbusername=$DATABASE_USERNAME \
      --dbpassword=$DATABASE_PASSWORD \
      --dbssl=$DATABASE_SSL && \
    strapi build && \
    yarn add strapi-provider-upload-aws-s3

VOLUME /srv/app

EXPOSE 1337

CMD ["strapi", "start"]
